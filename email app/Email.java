
package emailapp;
import java.util.Scanner;

public class Email {
	private String firstName;
	private String lastName;
	private String password;
	private int defltPasswordLength = 5;
	private String department;
	private int mailboxCapacity;
	private String alternateEmail;
	private String email;
	private String companyName = "google.com";
	
	// Constructor to receive firstName and lastName
	public Email(String firstName,String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
		System.out.println("EMAIL CREATED:" + firstName + " " + lastName);
		
		//Call a method for Department which will return department
		this.department = setDepartment();
		System.out.println("Department is: " + this.department);
		
		//Call a method that returns a random password
		this.password = randomPassword(defltPasswordLength);
		System.out.println("Random password is: " + this.password);
		
		//Combine elements to generate an email
		email = firstName.toLowerCase() + "." + lastName.toLowerCase() + "@" + department + "." + companyName;
		System.out.println("EMAIL GENERATED IS: " + email);
		
		
	}
	
	// Ask for the department
	private String setDepartment() {
		System.out.println("DEPARTMENT CODES \n1 for Sales\n2 for Development\n3 for Accounting\n0 for None\nEnter the DEpartment Code:");
		Scanner sc = new Scanner(System.in);
		int DeptChoice = sc.nextInt();
		if(DeptChoice ==1) {return "Sales"; }
		else if(DeptChoice ==2) {return "Development"; }
		else if(DeptChoice ==3) {return "Accounting"; }
		else {return "None"; }
	}
	
	// Generate a random password
	private String randomPassword(int length) {
		String passwordSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*";
			char[] password = new char[length];
			for(int i=0;i<length;i++) {
				int randm = (int)(Math.random() * passwordSet.length());
				password[i] = passwordSet.charAt(randm); 
			}
			return new String(password);
	}
	//Set the mailbox capacity
	public void setmailBoxCapacity(int capacity) {
		this.mailboxCapacity = capacity;
	}
	
	
	//Set the alternate Email address
	public void setAlternateemail(String Altemail) {
		this.alternateEmail = Altemail;
	}
	
	//Change the password
	public void changePassword(String password) {
		this.password = password;
	}
	
	public int getMailboxCapacity() {return mailboxCapacity; }
	public String getAlternateEmail() {return alternateEmail; }
	public String getPassword() {return password; }
	
}
